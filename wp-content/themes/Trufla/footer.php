<!--

These superfluous divs/spans were originally provided as catch-alls to add extra imagery.
These days we have full ::before and ::after support, favour using those instead.
These only remain for historical design compatibility. They might go away one day.

-->
<div class="extra1" role="presentation"></div><div class="extra2" role="presentation"></div><div class="extra3" role="presentation"></div>
<div class="extra4" role="presentation"></div><div class="extra5" role="presentation"></div><div class="extra6" role="presentation"></div>
<?php wp_footer(); ?>
</body>
</html>

// Acc
jQuery(document).on("click", ".naccs .menu div", function() {
	var numberIndex = jQuery(this).index();

	if (!jQuery(this).is("active")) {
		jQuery(".naccs .menu div").removeClass("active");
		jQuery(".naccs ul li").removeClass("active");

		jQuery(this).addClass("active");
		jQuery(".naccs ul").find("li:eq(" + numberIndex + ")").addClass("active");

		var listItemHeight = jQuery(".naccs ul")
			.find("li:eq(" + numberIndex + ")")
			.innerHeight();
		jQuery(".naccs ul").height(listItemHeight + "px");
	}
});

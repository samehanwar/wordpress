<?php

 function theme_widgets(){
    register_sidebar(
      array(
        'name' => 'sidebar',
        'id' => 'sidebar-1',
        'class' => 'sidebar-widget',
        'description' => 'main sidebar',
        'before_widget' => '<div id="design-archives" class="design-archives widget %2$s"><nav>',
        'after_widget' => "</nav></div>\n",
        'before_title' => '<h3 class="archives">',
        'after_title' => "</h3>\n"
      )
    );

    register_sidebar(
      array(
        'name' => 'sidebar-menu',
        'id' => 'sidebar-menu-1',
        'class' => 'sidebar-widget',
        'description' => 'main sidebar',
        'before_widget' => '<div id="zen-resources" class="zen-resources widget %2$s">',
        'after_widget' => "</div>\n",
        'before_title' => '<h2 class="resources">',
        'after_title' => "</h2>\n"
      )
    );

    register_sidebar(
      array(
        'name' => 'sidebar-links',
        'id' => 'sidebar-links-1',
        'class' => '',
        'description' => 'main sidebar',
        'before_widget' => '<div>',
        'after_widget' => "</div>\n"
      )
    );
  }

add_action('widgets_init', 'theme_widgets');

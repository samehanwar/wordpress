<?php

  add_theme_support('custom-background');
  add_theme_support('custom-header');
  add_theme_support('post-thumbnails');
  add_theme_support('post-formats', array('aside','quote'));

  function theme_init(){
    add_theme_support('menus');
    register_nav_menu('primary' , 'Primary Navigation Menu');
    register_nav_menu('secondary' , 'sidebar Navigation Menu');
  }
  add_action('init', 'theme_init');

  function theme_add_custom_menu_page()
  {
    add_menu_page( 'Post contents','contents','manage_options','post.php?post=173&action=edit','','',6 );
  }
  add_action('admin_menu', 'theme_add_custom_menu_page');



  function theme_custom_post()
  {
    $labels = array(
        'name'                => __( 'Header'),
        'singular_name'       => __( 'Headers')
    );
    $args_app = array(
        'label'               => __( 'Header' ),
        'description'         => __( 'Header information' ),
        'labels'              => $labels,
        'supports'            => falase,
        'hierarchical'        => false,
        'public'              => false,
        'show_ui'             => true,
        'show_in_menu'        => 'contents',
        'has_archive'         => false,
        'rewrite'             => false,
        'show_in_nav_menus'   => false,
        'exclude_from_search' => true,
        'menu_position'       => 5,
        'map_meta_cap' => true,
        'capability_type' => 'post',
        'capabilities' => array(
            'create_posts' => false
        )
    );
    register_post_type( 'Headers', $args_app );
  }
  add_action( 'init', 'theme_custom_post');

<?php
/**
 *  adding assets files for the website interface
 *
 */

function theme_load_scripts()
{
    wp_enqueue_style('style', get_template_directory_uri() . '/assets/css/style.css',array(),'1.0','');
}

function theme_load_admin_scripts()
{
    wp_register_style('tab',  get_template_directory_uri() . '/assets/css/tab.css' ,array(),'1.0.0','');
    wp_enqueue_style('tab');
    wp_register_script('index',  get_template_directory_uri() . '/assets/js/index.js' ,array('jquery'),'1.0.0',true);
    wp_enqueue_script('index');
}

add_action('wp_enqueue_scripts' ,'theme_load_scripts');
add_action('admin_enqueue_scripts','theme_load_admin_scripts');

<?php


  function wporg_add_custom_box()
  {
        add_meta_box(
            'wporg_box_id',           // Unique ID
            'Site Options Component',  // Box title
            'wporg_custom_box_html',  // Content callback, must be of type callable
            'Headers'                   // Post type
        );
  }
  add_action('add_meta_boxes', 'wporg_add_custom_box');

  function wporg_custom_box_html($post)
  {
    $value = get_post_meta($post->ID, '_wporg_meta_key', true);
      ?>
       <div class="naccs">
          <div class="grid">
             <div class="gc gc--1-of-3">
              <div class="menu">
               <div class="active"><span class="light"></span><span>Header Logo</span></div>
               <div><span class="light"></span><span>Footer Links</span></div>
               <div><span class="light"></span><span>others</span></div>
              </div>
             </div>
             <div class="gc gc--2-of-3">
              <ul class="nacc">
               <li class="active">
                 <div class="">
                      <label> Site Logo: </albel>
                      <input type="text" class="form-control" name="logo" value="<?php echo $value['logo']; ?>" />
                      <hr/>

                      <label> Site Brand Name: </albel>
                      <input type="text" class="form-control" name="brand" value="<?php echo $value['brand']; ?>" />

                      <label> Site Slogan: </albel>
                      <input type="text" class="form-control" name="slogan" value="<?php echo $value['slogan']; ?>" />
                 </div>
               </li>
               <li>
                 <div class="">
                   <div class="">
                        <label> Social Link: </albel>
                        <input type="text" class="form-control" name="link" value="<?php echo $value['link']; ?>" />

                        <label> Social Icon: <i> eg. fa fa-icon </i></albel>
                        <input type="text" class="form-control" name="icon" value="<?php echo $value['icon']; ?>" />

                        <label> label: </albel>
                        <input type="text" class="form-control" name="label" value="<?php echo $value['label']; ?>" />
                   </div>

                 </div>
               </li>
               <li>
                 <p> empty </p>
               </li>
              </ul>
             </div>
          </div>
        </div>
      <?php
  }

  function wporg_save_postdata($post_id)
{
    if (array_key_exists('brand', $_POST)) {
        update_post_meta(
            $post_id,
            '_wporg_meta_key',
              $_POST

        );
    }
}
add_action('save_post', 'wporg_save_postdata');

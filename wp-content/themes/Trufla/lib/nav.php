<?php
/*
 * @package App4Legal
 * Law Management Solution App
 * app4legal main navigation menu
 */

class App4legal_Nav_Walker extends Walker_Nav_Menu {
    function check_current($classes)
    {
      return preg_match('/(current[-_])|active|dropdown/', $classes);
    }

    function start_lvl(&$output, $depth = 0, $args = array())
    {
      $output .= "\n<ul class=\"dropdown-menu\">\n";
    }

    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
    {
        $item_html = '';
        parent::start_el($item_html, $item, $depth, $args);
        if ($item->is_dropdown && ($depth === 0 || $depth === 1)) {
            $item_html = str_replace('<a', '<a class="dropdown-toggle"', $item_html);
            $item_html = str_replace('</a>', ' <b class="caret"></b></a>', $item_html);
            if ($item->is_dropdown && ($depth === 1)) {
                $item_html = str_replace('<b class="caret"></b>', ' <b class="caret-right"></b>', $item_html);
                $item_html = str_replace('<li', '<li class="dropdown dropdown-submenu"', $item_html);
            }
        }
        elseif (stristr($item_html, 'li class="divider')) {
          $item_html = preg_replace('/<a[^>]*>.*?<\/a>/iU', '', $item_html);
        }
        elseif (stristr($item_html, 'li class="dropdown-header')) {
          $item_html = preg_replace('/<a[^>]*>(.*)<\/a>/iU', '$1', $item_html);
        }

        $item_html = apply_filters('roots_wp_nav_menu_item', $item_html);
        $output .= $item_html;
    }

    function display_element($element, &$children_elements, $max_depth, $depth = 0, $args, &$output) {
      $element->is_dropdown = ((!empty($children_elements[$element->ID]) && (($depth + 1) < $max_depth || ($max_depth === 0))));
      if ($element->is_dropdown) {
          $element->classes = array_merge( (array) $element->classes, array( 'dropdown' ) );
      }
      parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
    }
}
    function app4legal_nav_menu_css_class($classes, $item)
    {
      $slug = sanitize_title($item->title);
      $classes = preg_replace('/(current(-menu-|[-_]page[-_])(item|parent|ancestor))/', 'active', $classes);
      $classes = preg_replace('/^((menu|page)[-_\w+]+)+/', '', $classes);

      $classes[] = 'menu-' . $slug;

      $classes = array_unique($classes);

      return array_filter($classes, 'app4legal_is_element_empty');
    }
    add_filter('nav_menu_css_class', 'app4legal_nav_menu_css_class', 10, 2);
    add_filter('nav_menu_item_id', '__return_null');

    function app4legal_nav_menu_args($args = '') {
        $ap4l_nav_menu_args['container'] = false;
        if (!$args['items_wrap']) {
          $ap4l_nav_menu_args['items_wrap'] = '<ul class="%2$s">%3$s</ul>';
        }
        if (current_theme_supports('bootstrap-top-navbar') && !$args['depth']) {
          $ap4l_nav_menu_args['depth'] = 3;
        }
        if (!$args['walker']) {
          $ap4l_nav_menu_args['walker'] = new App4legal_Nav_Walker();
        }
        return array_merge($args, $ap4l_nav_menu_args);
    }
    add_filter('wp_nav_menu_args', 'app4legal_nav_menu_args');

    function app4legal_is_element_empty($element)
    {
        $element = trim($element);
        return empty($element) ? false : true;
    }

<div class="<?php echo getClassName(get_the_category()); ?>" id="zen-<?php echo getClassName(get_the_category()); ?>" role="article">
  <h3> <?php the_title(); ?> </h3>
  <?php the_content(); ?>
</div>

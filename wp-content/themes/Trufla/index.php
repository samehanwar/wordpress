<?php get_header() ?>
<?php   $data = get_post_meta(173, '_wporg_meta_key'); ?>

<div class="page-wrapper">

     <section class="intro" id="zen-intro">
        <header role="banner">
          <img src="<?php echo $data[0]['logo']; ?>" width="150" style="position: relative;top:18px;left:60px;display:inline-block;"/>
          <span style="display:inline-block;margin-left:-70px;">
            <h1><?php echo $data[0]['brand']; ?></h1>
            <h2><?php echo $data[0]['slogan']; ?></h2>
         </span>
        </header>




         <?php
          if(have_posts()):
              while(have_posts()): the_post();
                  if(get_post_format() == 'quote'){
                      get_template_part('content');
                  }
              endwhile;
            endif;
          ?>
      </section>

      <div class="main supporting" id="zen-supporting" role="main">

          <?php
            if(have_posts()):
                while(have_posts()): the_post();
                    if(get_post_format() == false){
                        get_template_part('content');
                    }
                endwhile;
              endif;
            ?>

            <footer>
              <a href="http://validator.w3.org/check/refere" title="Check the validity of this site&#8217;s HTML" class="zen-validate-html">HTML</a>
              <a href="http://jigsaw.w3.org/css-validator/check/referer" title="Check the validity of this site&#8217;s CSS" class="zen-validate-css">CSS</a>
              <a href="http://creativecommons.org/licenses/by-nc-sa/3.0/" title="View the Creative Commons license of this site: Attribution-NonCommercial-ShareAlike." class="zen-license">CC</a>
              <a href="http://mezzoblue.com/zengarden/faq/#aaa" title="Read about the accessibility of this site" class="zen-accessibility">A11y</a>
              <a href="https://github.com/mezzoblue/csszengarden.com" title="Fork this site on Github" class="zen-github">GH</a>
            </footer>

      </div>


      <aside class="sidebar" role="complementary">
          <div class="wrapper">

            <div class="design-selection" id="design-selection">
               <h3 class="select">Select a Design:</h3>
           		 <nav role="navigation">
           				<ul>
                    <?php
                      if(have_posts()):
                          while(have_posts()): the_post();
                              if(get_post_format() == 'aside'){
                                  get_template_part( 'content', 'aside' );
                              }
                            endwhile;
                        endif;
                      ?>
                  </ul>
               </nav>
            </div>

            <?php get_sidebar(); ?>

          </div>
       </aside>

</div>

<?php get_footer(); ?>

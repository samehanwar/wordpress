-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 30, 2018 at 02:50 PM
-- Server version: 5.7.21
-- PHP Version: 7.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wordpress`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

DROP TABLE IF EXISTS `wp_commentmeta`;
CREATE TABLE IF NOT EXISTS `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

DROP TABLE IF EXISTS `wp_comments`;
CREATE TABLE IF NOT EXISTS `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2018-10-28 11:10:07', '2018-10-28 11:10:07', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, 'post-trashed', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

DROP TABLE IF EXISTS `wp_links`;
CREATE TABLE IF NOT EXISTS `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

DROP TABLE IF EXISTS `wp_options`;
CREATE TABLE IF NOT EXISTS `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=MyISAM AUTO_INCREMENT=244 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/wordpress', 'yes'),
(2, 'home', 'http://localhost/wordpress', 'yes'),
(3, 'blogname', 'theme Dev', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'samohero12@yahoo.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '50', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '50', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:86:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:0:{}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'Trufla', 'yes'),
(41, 'stylesheet', 'Trufla', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '0', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '0', 'yes'),
(93, 'initial_db_version', '38590', 'yes'),
(94, 'wp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'sidebars_widgets', 'a:5:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-1\";a:1:{i:0;s:10:\"nav_menu-3\";}s:14:\"sidebar-menu-1\";a:1:{i:0;s:10:\"nav_menu-2\";}s:15:\"sidebar-links-1\";a:1:{i:0;s:13:\"custom_html-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(102, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_nav_menu', 'a:3:{i:2;a:1:{s:8:\"nav_menu\";i:8;}i:3;a:1:{s:8:\"nav_menu\";i:7;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_custom_html', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:7:\"content\";s:785:\"              <a href=\"http://validator.w3.org/check/refere\" title=\"Check the validity of this site&#8217;s HTML\" class=\"zen-validate-html\">HTML</a>\r\n              <a href=\"http://jigsaw.w3.org/css-validator/check/referer\" title=\"Check the validity of this site&#8217;s CSS\" class=\"zen-validate-css\">CSS</a>\r\n              <a href=\"http://creativecommons.org/licenses/by-nc-sa/3.0/\" title=\"View the Creative Commons license of this site: Attribution-NonCommercial-ShareAlike.\" class=\"zen-license\">CC</a>\r\n              <a href=\"http://mezzoblue.com/zengarden/faq/#aaa\" title=\"Read about the accessibility of this site\" class=\"zen-accessibility\">A11y</a>\r\n              <a href=\"https://github.com/mezzoblue/csszengarden.com\" title=\"Fork this site on Github\" class=\"zen-github\">GH</a>\r\n\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'cron', 'a:5:{i:1540829407;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1540854607;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1540897823;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1540912399;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(112, 'theme_mods_twentyseventeen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1540726554;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes'),
(116, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.9.8.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.9.8.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-4.9.8-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-4.9.8-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"4.9.8\";s:7:\"version\";s:5:\"4.9.8\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"4.7\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1540817687;s:15:\"version_checked\";s:5:\"4.9.8\";s:12:\"translations\";a:0:{}}', 'no'),
(118, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1540817691;s:7:\"checked\";a:2:{s:19:\"akismet/akismet.php\";s:5:\"4.0.8\";s:9:\"hello.php\";s:3:\"1.7\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:2:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.0.8\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.0.8.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=969907\";s:2:\"1x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=969907\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:65:\"https://ps.w.org/hello-dolly/assets/banner-772x250.png?rev=478342\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(239, '_site_transient_timeout_theme_roots', '1540819490', 'no'),
(240, '_site_transient_theme_roots', 'a:4:{s:6:\"Trufla\";s:7:\"/themes\";s:13:\"twentyfifteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:13:\"twentysixteen\";s:7:\"/themes\";}', 'no'),
(121, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1540817691;s:7:\"checked\";a:4:{s:6:\"Trufla\";s:3:\"1.0\";s:13:\"twentyfifteen\";s:3:\"2.0\";s:15:\"twentyseventeen\";s:3:\"1.7\";s:13:\"twentysixteen\";s:3:\"1.5\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(122, '_site_transient_timeout_browser_90ff8ae6231a43c42b418e1765751722', '1541329824', 'no'),
(123, '_site_transient_browser_90ff8ae6231a43c42b418e1765751722', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:12:\"70.0.3538.77\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(127, 'can_compress_scripts', '1', 'no'),
(138, 'current_theme', 'Trufla', 'yes'),
(139, 'theme_mods_Trufla', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:2:{s:7:\"primary\";i:8;s:9:\"secondary\";i:7;}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1540741291;s:4:\"data\";a:1:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(140, 'theme_switched', '', 'yes'),
(224, 'category_children', 'a:0:{}', 'yes'),
(157, 'theme_mods_twentyfifteen', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1540741315;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-1\";a:0:{}}}}', 'yes'),
(181, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(159, '_transient_twentyfifteen_categories', '1', 'yes'),
(161, 'theme_mods_twentysixteen', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1540741346;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-1\";a:0:{}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes'),
(162, '_transient_twentysixteen_categories', '1', 'yes'),
(210, '_transient_timeout_plugin_slugs', '1540855175', 'no'),
(211, '_transient_plugin_slugs', 'a:3:{i:0;s:30:\"advanced-custom-fields/acf.php\";i:1;s:19:\"akismet/akismet.php\";i:2;s:9:\"hello.php\";}', 'no'),
(212, 'recently_activated', 'a:1:{s:30:\"advanced-custom-fields/acf.php\";i:1540768775;}', 'yes'),
(213, 'acf_version', '5.7.7', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

DROP TABLE IF EXISTS `wp_postmeta`;
CREATE TABLE IF NOT EXISTS `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=336 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 2, '_edit_lock', '1540738834:1'),
(4, 2, '_edit_last', '1'),
(5, 6, '_edit_last', '1'),
(10, 8, '_edit_lock', '1540747552:1'),
(9, 8, '_edit_last', '1'),
(8, 6, '_edit_lock', '1540747347:1'),
(13, 1, '_edit_lock', '1540739921:1'),
(18, 1, '_wp_trash_meta_status', 'publish'),
(19, 1, '_wp_trash_meta_time', '1540743690'),
(20, 1, '_wp_desired_post_slug', 'hello-world'),
(21, 1, '_wp_trash_meta_comments_status', 'a:1:{i:1;s:1:\"1\";}'),
(22, 11, '_edit_last', '1'),
(23, 11, '_edit_lock', '1540785499:1'),
(27, 13, '_edit_lock', '1540757706:1'),
(26, 13, '_edit_last', '1'),
(43, 16, '_edit_lock', '1540748146:1'),
(42, 16, '_edit_last', '1'),
(44, 18, '_edit_last', '1'),
(45, 18, '_edit_lock', '1540748165:1'),
(46, 20, '_edit_last', '1'),
(47, 20, '_edit_lock', '1540748181:1'),
(48, 22, '_edit_last', '1'),
(49, 22, '_edit_lock', '1540748197:1'),
(50, 24, '_edit_last', '1'),
(51, 24, '_edit_lock', '1540748204:1'),
(52, 26, '_edit_last', '1'),
(53, 26, '_edit_lock', '1540748220:1'),
(54, 28, '_edit_last', '1'),
(55, 28, '_edit_lock', '1540748235:1'),
(56, 30, '_edit_last', '1'),
(57, 30, '_edit_lock', '1540748249:1'),
(58, 3, '_wp_trash_meta_status', 'draft'),
(59, 3, '_wp_trash_meta_time', '1540748401'),
(60, 3, '_wp_desired_post_slug', 'privacy-policy'),
(140, 47, '_edit_lock', '1540748617:1'),
(333, 173, '_edit_lock', '1540825998:1'),
(139, 47, '_edit_last', '1'),
(332, 173, '_wporg_meta_key', 'a:48:{s:8:\"_wpnonce\";s:10:\"b100444f21\";s:16:\"_wp_http_referer\";s:59:\"/wordpress/wp-admin/post.php?post=173&action=edit&message=1\";s:7:\"user_ID\";i:1;s:6:\"action\";s:8:\"editpost\";s:14:\"originalaction\";s:8:\"editpost\";s:11:\"post_author\";i:1;s:9:\"post_type\";s:7:\"headers\";s:20:\"original_post_status\";s:7:\"publish\";s:10:\"referredby\";s:65:\"http://localhost/wordpress/wp-admin/post.php?post=173&action=edit\";s:25:\"_wp_original_http_referer\";s:65:\"http://localhost/wordpress/wp-admin/post.php?post=173&action=edit\";s:7:\"post_ID\";s:3:\"173\";s:20:\"meta-box-order-nonce\";s:10:\"f7ceb0dfec\";s:20:\"closedpostboxesnonce\";s:10:\"5a37930403\";s:18:\"hidden_post_status\";s:7:\"publish\";s:11:\"post_status\";s:7:\"publish\";s:20:\"hidden_post_password\";s:0:\"\";s:22:\"hidden_post_visibility\";s:6:\"public\";s:10:\"visibility\";s:6:\"public\";s:13:\"post_password\";s:0:\"\";s:2:\"mm\";s:2:\"10\";s:2:\"jj\";s:2:\"29\";s:2:\"aa\";s:4:\"2018\";s:2:\"hh\";s:2:\"13\";s:2:\"mn\";s:2:\"39\";s:2:\"ss\";s:2:\"04\";s:9:\"hidden_mm\";s:2:\"10\";s:6:\"cur_mm\";s:2:\"10\";s:9:\"hidden_jj\";s:2:\"29\";s:6:\"cur_jj\";s:2:\"29\";s:9:\"hidden_aa\";s:4:\"2018\";s:6:\"cur_aa\";s:4:\"2018\";s:9:\"hidden_hh\";s:2:\"13\";s:6:\"cur_hh\";s:2:\"14\";s:9:\"hidden_mn\";s:2:\"39\";s:6:\"cur_mn\";s:2:\"30\";s:16:\"original_publish\";s:6:\"Update\";s:4:\"save\";s:6:\"Update\";s:9:\"post_name\";s:3:\"173\";s:4:\"logo\";s:62:\"http://localhost/wordpress/wp-content/uploads/2018/10/enso.png\";s:5:\"brand\";s:14:\"CSS Zen Garden\";s:6:\"slogan\";s:24:\"The Beauty of CSS Design\";s:4:\"link\";s:19:\"site name -- tr0044\";s:4:\"icon\";s:19:\"site name -- tr0044\";s:5:\"label\";s:19:\"site name -- tr0044\";s:14:\"post_mime_type\";s:0:\"\";s:2:\"ID\";i:173;s:14:\"comment_status\";s:6:\"closed\";s:11:\"ping_status\";s:6:\"closed\";}'),
(331, 173, '_edit_last', '1'),
(330, 168, '_edit_lock', '1540820135:1'),
(329, 168, '_wporg_meta_key', 'site name -- tr0044'),
(328, 168, '_edit_last', '1'),
(138, 45, '_edit_lock', '1540748610:1'),
(327, 160, '_wp_desired_post_slug', '160'),
(326, 160, '_wp_trash_meta_time', '1540819559'),
(325, 160, '_wp_trash_meta_status', 'publish'),
(324, 160, '_edit_lock', '1540819402:1'),
(137, 45, '_edit_last', '1'),
(323, 160, '_wporg_meta_key', 'a:2:{i:0;s:17:\"site name -- tr00\";i:1;s:18:\"slogan site title \";}'),
(322, 160, '_edit_last', '1'),
(319, 158, '_edit_last', '1'),
(320, 158, '_edit_lock', '1540819873:1'),
(317, 157, '_menu_item_url', 'http://www.mezzoblue.com/zengarden/alldesigns/'),
(316, 157, '_menu_item_xfn', ''),
(136, 43, '_edit_lock', '1540748591:1'),
(315, 157, '_menu_item_classes', 'a:1:{i:0;s:7:\"viewall\";}'),
(314, 157, '_menu_item_target', ''),
(313, 157, '_menu_item_object', 'custom'),
(312, 157, '_menu_item_object_id', '157'),
(311, 157, '_menu_item_menu_item_parent', '0'),
(135, 43, '_edit_last', '1'),
(310, 157, '_menu_item_type', 'custom'),
(321, 158, '_wporg_meta_key', 'site name -- tr00'),
(308, 156, '_menu_item_url', 'http://www.mezzoblue.com/214/'),
(307, 156, '_menu_item_xfn', ''),
(306, 156, '_menu_item_classes', 'a:1:{i:0;s:4:\"next\";}'),
(134, 41, '_edit_lock', '1540748571:1'),
(305, 156, '_menu_item_target', ''),
(304, 156, '_menu_item_object', 'custom'),
(303, 156, '_menu_item_object_id', '156'),
(302, 156, '_menu_item_menu_item_parent', '0'),
(301, 156, '_menu_item_type', 'custom'),
(133, 41, '_edit_last', '1'),
(141, 49, '_edit_last', '1'),
(142, 49, '_edit_lock', '1540748636:1'),
(143, 51, '_menu_item_type', 'post_type'),
(144, 51, '_menu_item_menu_item_parent', '0'),
(145, 51, '_menu_item_object_id', '49'),
(146, 51, '_menu_item_object', 'page'),
(147, 51, '_menu_item_target', ''),
(148, 51, '_menu_item_classes', 'a:1:{i:0;s:16:\"zen-translations\";}'),
(149, 51, '_menu_item_xfn', ''),
(150, 51, '_menu_item_url', ''),
(152, 52, '_menu_item_type', 'post_type'),
(153, 52, '_menu_item_menu_item_parent', '0'),
(154, 52, '_menu_item_object_id', '47'),
(155, 52, '_menu_item_object', 'page'),
(156, 52, '_menu_item_target', ''),
(157, 52, '_menu_item_classes', 'a:1:{i:0;s:8:\"view-css\";}'),
(158, 52, '_menu_item_xfn', ''),
(159, 52, '_menu_item_url', ''),
(161, 53, '_menu_item_type', 'post_type'),
(162, 53, '_menu_item_menu_item_parent', '0'),
(163, 53, '_menu_item_object_id', '45'),
(164, 53, '_menu_item_object', 'page'),
(165, 53, '_menu_item_target', ''),
(166, 53, '_menu_item_classes', 'a:1:{i:0;s:10:\"zen-submit\";}'),
(167, 53, '_menu_item_xfn', ''),
(168, 53, '_menu_item_url', ''),
(170, 54, '_menu_item_type', 'post_type'),
(171, 54, '_menu_item_menu_item_parent', '0'),
(172, 54, '_menu_item_object_id', '43'),
(173, 54, '_menu_item_object', 'page'),
(174, 54, '_menu_item_target', ''),
(175, 54, '_menu_item_classes', 'a:1:{i:0;s:7:\"zen-faq\";}'),
(176, 54, '_menu_item_xfn', ''),
(177, 54, '_menu_item_url', ''),
(179, 55, '_menu_item_type', 'post_type'),
(180, 55, '_menu_item_menu_item_parent', '0'),
(181, 55, '_menu_item_object_id', '41'),
(182, 55, '_menu_item_object', 'page'),
(183, 55, '_menu_item_target', ''),
(184, 55, '_menu_item_classes', 'a:1:{i:0;s:13:\"css-resources\";}'),
(185, 55, '_menu_item_xfn', ''),
(186, 55, '_menu_item_url', ''),
(188, 56, '_edit_last', '1'),
(189, 56, '_edit_lock', '1540759821:1'),
(190, 56, 'author', 'Andrew Lohman'),
(196, 59, '_edit_lock', '1540759853:1'),
(195, 59, '_edit_last', '1'),
(197, 59, 'author', 'Dan Mall'),
(201, 61, '_edit_lock', '1540759888:1'),
(200, 61, '_edit_last', '1'),
(202, 61, 'author', 'Steffen Knoeller'),
(203, 61, 'author link', '\'\''),
(207, 64, '_edit_lock', '1540759919:1'),
(206, 64, '_edit_last', '1'),
(208, 64, 'author', 'Trent Walton'),
(212, 66, '_edit_lock', '1540759947:1'),
(211, 66, '_edit_last', '1'),
(213, 66, 'author', 'Elliot Jay Stocks'),
(222, 68, '_edit_last', '1'),
(223, 68, '_edit_lock', '1540760022:1'),
(224, 68, 'author', 'Dave Shea'),
(225, 68, 'author_link', '\'\''),
(229, 70, '_edit_lock', '1540760003:1'),
(228, 70, '_edit_last', '1'),
(230, 70, 'author', 'meltmedia '),
(231, 70, 'author_link', '\'\''),
(234, 73, 'author', 'Jeremy Carlson'),
(235, 73, '_edit_last', '1'),
(236, 73, '_edit_lock', '1540766126:1'),
(238, 73, 'author_link', '\'\''),
(241, 56, 'author_link', '\'\''),
(244, 59, 'author_link', '\'\''),
(262, 28, '_wp_trash_meta_time', '1540760330'),
(249, 64, 'author_link', '\'\''),
(252, 66, 'author_link', '\'\''),
(261, 28, '_wp_trash_meta_status', 'publish'),
(263, 28, '_wp_desired_post_slug', 'a-robot-named-jimmy'),
(264, 22, '_wp_trash_meta_status', 'publish'),
(265, 22, '_wp_trash_meta_time', '1540760330'),
(266, 22, '_wp_desired_post_slug', 'apothecary'),
(267, 26, '_wp_trash_meta_status', 'publish'),
(268, 26, '_wp_trash_meta_time', '1540760330'),
(269, 26, '_wp_desired_post_slug', 'fountain-kiss'),
(270, 18, '_wp_trash_meta_status', 'publish'),
(271, 18, '_wp_trash_meta_time', '1540760330'),
(272, 18, '_wp_desired_post_slug', 'garments'),
(273, 16, '_wp_trash_meta_status', 'publish'),
(274, 16, '_wp_trash_meta_time', '1540760330'),
(275, 16, '_wp_desired_post_slug', 'mid-century-modern'),
(276, 24, '_wp_trash_meta_status', 'publish'),
(277, 24, '_wp_trash_meta_time', '1540760330'),
(278, 24, '_wp_desired_post_slug', 'screen-filler'),
(279, 20, '_wp_trash_meta_status', 'publish'),
(280, 20, '_wp_trash_meta_time', '1540760330'),
(281, 20, '_wp_desired_post_slug', 'steel'),
(282, 30, '_wp_trash_meta_status', 'publish'),
(283, 30, '_wp_trash_meta_time', '1540760330'),
(284, 30, '_wp_desired_post_slug', 'verde-moderna'),
(285, 147, '_edit_last', '1'),
(290, 149, '_edit_lock', '1540784764:1'),
(289, 149, '_edit_last', '1'),
(288, 147, '_edit_lock', '1540820149:1'),
(334, 175, '_wp_attached_file', '2018/10/enso.png'),
(335, 175, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:369;s:6:\"height\";i:373;s:4:\"file\";s:16:\"2018/10/enso.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"enso-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"enso-297x300.png\";s:5:\"width\";i:297;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

DROP TABLE IF EXISTS `wp_posts`;
CREATE TABLE IF NOT EXISTS `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=MyISAM AUTO_INCREMENT=176 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2018-10-28 11:10:07', '2018-10-28 11:10:07', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'trash', 'open', 'open', '', 'hello-world__trashed', '', '', '2018-10-28 16:21:30', '2018-10-28 16:21:30', '', 0, 'http://localhost/wordpress/?p=1', 0, 'post', '', 1),
(2, 1, '2018-10-28 11:10:07', '2018-10-28 11:10:07', 'This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\r\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like piña coladas. (And gettin\' caught in the rain.)</blockquote>\r\n...or something like this:\r\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\r\nAs a new WordPress user, you should go to <a href=\"http://localhost/wordpress/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Home Page', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2018-10-28 15:02:56', '2018-10-28 15:02:56', '', 0, 'http://localhost/wordpress/?page_id=2', 0, 'page', '', 0),
(3, 1, '2018-10-28 11:10:07', '2018-10-28 11:10:07', '<h2>Who we are</h2><p>Our website address is: http://localhost/wordpress.</p><h2>What personal data we collect and why we collect it</h2><h3>Comments</h3><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><h3>Media</h3><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><h3>Contact forms</h3><h3>Cookies</h3><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><p>If you have an account and you log in to this site, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><h3>Embedded content from other websites</h3><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><h3>Analytics</h3><h2>Who we share your data with</h2><h2>How long we retain your data</h2><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><h2>What rights you have over your data</h2><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><h2>Where we send your data</h2><p>Visitor comments may be checked through an automated spam detection service.</p><h2>Your contact information</h2><h2>Additional information</h2><h3>How we protect your data</h3><h3>What data breach procedures we have in place</h3><h3>What third parties we receive data from</h3><h3>What automated decision making and/or profiling we do with user data</h3><h3>Industry regulatory disclosure requirements</h3>', 'Privacy Policy', '', 'trash', 'closed', 'open', '', 'privacy-policy__trashed', '', '', '2018-10-28 17:40:01', '2018-10-28 17:40:01', '', 0, 'http://localhost/wordpress/?page_id=3', 0, 'page', '', 0),
(4, 1, '2018-10-28 11:10:24', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-10-28 11:10:24', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?p=4', 0, 'post', '', 0),
(5, 1, '2018-10-28 15:02:56', '2018-10-28 15:02:56', 'This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\r\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like piña coladas. (And gettin\' caught in the rain.)</blockquote>\r\n...or something like this:\r\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\r\nAs a new WordPress user, you should go to <a href=\"http://localhost/wordpress/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Home Page', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2018-10-28 15:02:56', '2018-10-28 15:02:56', '', 2, 'http://localhost/wordpress/2-revision-v1/', 0, 'revision', '', 0),
(6, 1, '2018-10-28 14:14:17', '2018-10-28 14:14:17', 'Strong visual design has always been our focus. You are modifying this page, so strong <abbr title=\"Cascading Style Sheets\">CSS</abbr> skills are necessary too, but the example files are commented well enough that even <abbr title=\"Cascading Style Sheets\">CSS</abbr> novices can use them as starting points. Please see the <a title=\"A listing of CSS-related resources\" href=\"http://www.mezzoblue.com/zengarden/resources/\"><abbr title=\"Cascading Style Sheets\">CSS</abbr> Resource Guide</a> for advanced tutorials and tips on working with <abbr title=\"Cascading Style Sheets\">CSS</abbr>.\r\n\r\nYou may modify the style sheet in any way you wish, but not the <abbr title=\"HyperText Markup Language\">HTML</abbr>. This may seem daunting at first if you’ve never worked this way before, but follow the listed links to learn more, and use the sample files as a guide.\r\n\r\nDownload the sample <a title=\"This page\'s source HTML code, not to be modified.\" href=\"/examples/index\">HTML</a> and <a title=\"This page\'s sample CSS, the file you may modify.\" href=\"/examples/style.css\">CSS</a> to work on a copy locally. Once you have completed your masterpiece (and please, don’t submit half-finished work) upload your <abbr title=\"Cascading Style Sheets\">CSS</abbr> file to a web server under your control. <a title=\"Use the contact form to send us your CSS file\" href=\"http://www.mezzoblue.com/zengarden/submit/\">Send us a link</a> to an archive of that file and all associated assets, and if we choose to use it we will download it and place it on our server.', 'Participation', '', 'publish', 'open', 'open', '', 'participation', '', '', '2018-10-28 17:24:48', '2018-10-28 17:24:48', '', 0, 'http://localhost/wordpress/?p=6', 0, 'post', '', 0),
(7, 1, '2018-10-28 15:14:17', '2018-10-28 15:14:17', 'Strong visual design has always been our focus. You are modifying this page, so strong <abbr title=\"Cascading Style Sheets\">CSS</abbr> skills are necessary too, but the example files are commented well enough that even <abbr title=\"Cascading Style Sheets\">CSS</abbr> novices can use them as starting points. Please see the <a title=\"A listing of CSS-related resources\" href=\"http://www.mezzoblue.com/zengarden/resources/\"><abbr title=\"Cascading Style Sheets\">CSS</abbr> Resource Guide</a> for advanced tutorials and tips on working with <abbr title=\"Cascading Style Sheets\">CSS</abbr>.\r\n\r\nYou may modify the style sheet in any way you wish, but not the <abbr title=\"HyperText Markup Language\">HTML</abbr>. This may seem daunting at first if you’ve never worked this way before, but follow the listed links to learn more, and use the sample files as a guide.\r\n\r\nDownload the sample <a title=\"This page\'s source HTML code, not to be modified.\" href=\"/examples/index\">HTML</a> and <a title=\"This page\'s sample CSS, the file you may modify.\" href=\"/examples/style.css\">CSS</a> to work on a copy locally. Once you have completed your masterpiece (and please, don’t submit half-finished work) upload your <abbr title=\"Cascading Style Sheets\">CSS</abbr> file to a web server under your control. <a title=\"Use the contact form to send us your CSS file\" href=\"http://www.mezzoblue.com/zengarden/submit/\">Send us a link</a> to an archive of that file and all associated assets, and if we choose to use it we will download it and place it on our server.', 'Participation', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2018-10-28 15:14:17', '2018-10-28 15:14:17', '', 6, 'http://localhost/wordpress/6-revision-v1/', 0, 'revision', '', 0),
(8, 1, '2018-10-28 14:09:12', '2018-10-28 14:09:12', '<p>Why participate? For recognition, inspiration, and a resource we can all refer to showing people how amazing <abbr title=\"Cascading Style Sheets\">CSS</abbr> really can be. This site serves as equal parts inspiration for those working on the web today, learning tool for those who will be tomorrow, and gallery of future techniques we can all look forward to.</p>\r\n', 'Benefits', '', 'publish', 'open', 'open', '', 'benefits', '', '', '2018-10-28 17:25:52', '2018-10-28 17:25:52', '', 0, 'http://localhost/wordpress/?p=8', 0, 'post', '', 0),
(9, 1, '2018-10-28 15:16:12', '2018-10-28 15:16:12', '<p>Why participate? For recognition, inspiration, and a resource we can all refer to showing people how amazing <abbr title=\"Cascading Style Sheets\">CSS</abbr> really can be. This site serves as equal parts inspiration for those working on the web today, learning tool for those who will be tomorrow, and gallery of future techniques we can all look forward to.</p>\r\n', 'Benefits', '', 'inherit', 'closed', 'closed', '', '8-revision-v1', '', '', '2018-10-28 15:16:12', '2018-10-28 15:16:12', '', 8, 'http://localhost/wordpress/8-revision-v1/', 0, 'revision', '', 0),
(10, 1, '2018-10-28 16:21:30', '2018-10-28 16:21:30', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2018-10-28 16:21:30', '2018-10-28 16:21:30', '', 1, 'http://localhost/wordpress/1-revision-v1/', 0, 'revision', '', 0),
(11, 1, '2018-10-28 13:22:21', '2018-10-28 13:22:21', 'Where possible, we would like to see mostly  <abbr title=\"Cascading Style Sheets, levels 1 and 2\">CSS 1 &amp; 2</abbr> usage. <abbr title=\"Cascading Style Sheets, levels 3 and 4\">CSS 3 &amp; 4</abbr> should be limited to widely-supported elements only, or strong fallbacks should be provided. The CSS Zen Garden is about functional, practical <abbr title=\"Cascading Style Sheets\">CSS</abbr> and not the latest bleeding-edge tricks viewable by 2% of the browsing public. The only real requirement we have is that your <abbr title=\"Cascading Style Sheets\">CSS</abbr> validates.\r\n\r\nLuckily, designing this way shows how well various browsers have implemented <abbr title=\"Cascading Style Sheets\">CSS</abbr> by now. When sticking to the guidelines you should see fairly consistent results across most modern browsers. Due to the sheer number of user agents on the web these days — especially when you factor in mobile — pixel-perfect layouts may not be possible across every platform. That’s okay, but do test in as many as you can. Your design should work in at least IE9+ and the latest Chrome, Firefox, iOS and Android browsers (run by over 90% of the population).\r\n\r\nWe ask that you submit original artwork. Please respect copyright laws. Please keep objectionable material to a minimum, and try to incorporate unique and interesting visual themes to your work. We’re well past the point of needing another garden-related design.\r\n\r\nThis is a learning exercise as well as a demonstration. You retain full copyright on your graphics (with limited exceptions, see <a href=\"http://www.mezzoblue.com/zengarden/submit/guidelines/\">submission guidelines</a>), but we ask you release your <abbr title=\"Cascading Style Sheets\">CSS</abbr> under a Creative Commons license identical to the <a title=\"View the Zen Garden\'s license information.\" href=\"http://creativecommons.org/licenses/by-nc-sa/3.0/\">one on this site</a> so that others may learn from your work.\r\n<p role=\"contentinfo\">By <a href=\"http://www.mezzoblue.com/\">Dave Shea</a>. Bandwidth graciously donated by <a href=\"http://www.mediatemple.net/\">mediatemple</a>. Now available: <a href=\"http://www.amazon.com/exec/obidos/ASIN/0321303474/mezzoblue-20/\">Zen Garden, the book</a>.</p>', 'Requirements', '', 'publish', 'open', 'open', '', 'requirements', '', '', '2018-10-28 17:24:18', '2018-10-28 17:24:18', '', 0, 'http://localhost/wordpress/?p=11', 0, 'post', '', 0),
(12, 1, '2018-10-28 16:22:21', '2018-10-28 16:22:21', 'Where possible, we would like to see mostly <abbr title=\"Cascading Style Sheets, levels 1 and 2\">CSS 1 &amp; 2</abbr> usage. <abbr title=\"Cascading Style Sheets, levels 3 and 4\">CSS 3 &amp; 4</abbr> should be limited to widely-supported elements only, or strong fallbacks should be provided. The CSS Zen Garden is about functional, practical <abbr title=\"Cascading Style Sheets\">CSS</abbr> and not the latest bleeding-edge tricks viewable by 2% of the browsing public. The only real requirement we have is that your <abbr title=\"Cascading Style Sheets\">CSS</abbr> validates.\r\n\r\nLuckily, designing this way shows how well various browsers have implemented <abbr title=\"Cascading Style Sheets\">CSS</abbr> by now. When sticking to the guidelines you should see fairly consistent results across most modern browsers. Due to the sheer number of user agents on the web these days — especially when you factor in mobile — pixel-perfect layouts may not be possible across every platform. That’s okay, but do test in as many as you can. Your design should work in at least IE9+ and the latest Chrome, Firefox, iOS and Android browsers (run by over 90% of the population).\r\n\r\nWe ask that you submit original artwork. Please respect copyright laws. Please keep objectionable material to a minimum, and try to incorporate unique and interesting visual themes to your work. We’re well past the point of needing another garden-related design.\r\n\r\nThis is a learning exercise as well as a demonstration. You retain full copyright on your graphics (with limited exceptions, see <a href=\"http://www.mezzoblue.com/zengarden/submit/guidelines/\">submission guidelines</a>), but we ask you release your <abbr title=\"Cascading Style Sheets\">CSS</abbr> under a Creative Commons license identical to the <a title=\"View the Zen Garden\'s license information.\" href=\"http://creativecommons.org/licenses/by-nc-sa/3.0/\">one on this site</a> so that others may learn from your work.\r\n<p role=\"contentinfo\">By <a href=\"http://www.mezzoblue.com/\">Dave Shea</a>. Bandwidth graciously donated by <a href=\"http://www.mediatemple.net/\">mediatemple</a>. Now available: <a href=\"http://www.amazon.com/exec/obidos/ASIN/0321303474/mezzoblue-20/\">Zen Garden, the book</a>.</p>', 'Requirements', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2018-10-28 16:22:21', '2018-10-28 16:22:21', '', 11, 'http://localhost/wordpress/11-revision-v1/', 0, 'revision', '', 0),
(13, 1, '2018-10-28 16:23:22', '2018-10-28 16:23:22', 'There is a continuing need to show the power of <abbr title=\"Cascading Style Sheets\">CSS</abbr>. The Zen Garden aims to excite, inspire, and encourage participation. To begin, view some of the existing designs in the list. Clicking on any one will load the style sheet into this very page. The <abbr title=\"HyperText Markup Language\">HTML</abbr> remains the same, the only thing that has changed is the external <abbr title=\"Cascading Style Sheets\">CSS</abbr> file. Yes, really.\r\n\r\n<abbr title=\"Cascading Style Sheets\">CSS</abbr> allows complete and total control over the style of a hypertext document. The only way this can be illustrated in a way that gets people excited is by demonstrating what it can truly be, once the reins are placed in the hands of those able to create beauty from structure. Designers and coders alike have contributed to the beauty of the web; we can always push it further.', 'So What is This About?', '', 'publish', 'open', 'open', '', 'so-what-is-this-about', '', '', '2018-10-28 17:16:56', '2018-10-28 17:16:56', '', 0, 'http://localhost/wordpress/?p=13', 0, 'post', '', 0),
(14, 1, '2018-10-28 16:23:22', '2018-10-28 16:23:22', 'There is a continuing need to show the power of <abbr title=\"Cascading Style Sheets\">CSS</abbr>. The Zen Garden aims to excite, inspire, and encourage participation. To begin, view some of the existing designs in the list. Clicking on any one will load the style sheet into this very page. The <abbr title=\"HyperText Markup Language\">HTML</abbr> remains the same, the only thing that has changed is the external <abbr title=\"Cascading Style Sheets\">CSS</abbr> file. Yes, really.\r\n\r\n<abbr title=\"Cascading Style Sheets\">CSS</abbr> allows complete and total control over the style of a hypertext document. The only way this can be illustrated in a way that gets people excited is by demonstrating what it can truly be, once the reins are placed in the hands of those able to create beauty from structure. Designers and coders alike have contributed to the beauty of the web; we can always push it further.', 'So What is This About?', '', 'inherit', 'closed', 'closed', '', '13-revision-v1', '', '', '2018-10-28 16:23:22', '2018-10-28 16:23:22', '', 13, 'http://localhost/wordpress/13-revision-v1/', 0, 'revision', '', 0),
(15, 1, '2018-10-28 17:23:42', '2018-10-28 17:23:42', 'Where possible, we would like to see mostly  <abbr title=\"Cascading Style Sheets, levels 1 and 2\">CSS 1 &amp; 2</abbr> usage. <abbr title=\"Cascading Style Sheets, levels 3 and 4\">CSS 3 &amp; 4</abbr> should be limited to widely-supported elements only, or strong fallbacks should be provided. The CSS Zen Garden is about functional, practical <abbr title=\"Cascading Style Sheets\">CSS</abbr> and not the latest bleeding-edge tricks viewable by 2% of the browsing public. The only real requirement we have is that your <abbr title=\"Cascading Style Sheets\">CSS</abbr> validates.\r\n\r\nLuckily, designing this way shows how well various browsers have implemented <abbr title=\"Cascading Style Sheets\">CSS</abbr> by now. When sticking to the guidelines you should see fairly consistent results across most modern browsers. Due to the sheer number of user agents on the web these days — especially when you factor in mobile — pixel-perfect layouts may not be possible across every platform. That’s okay, but do test in as many as you can. Your design should work in at least IE9+ and the latest Chrome, Firefox, iOS and Android browsers (run by over 90% of the population).\r\n\r\nWe ask that you submit original artwork. Please respect copyright laws. Please keep objectionable material to a minimum, and try to incorporate unique and interesting visual themes to your work. We’re well past the point of needing another garden-related design.\r\n\r\nThis is a learning exercise as well as a demonstration. You retain full copyright on your graphics (with limited exceptions, see <a href=\"http://www.mezzoblue.com/zengarden/submit/guidelines/\">submission guidelines</a>), but we ask you release your <abbr title=\"Cascading Style Sheets\">CSS</abbr> under a Creative Commons license identical to the <a title=\"View the Zen Garden\'s license information.\" href=\"http://creativecommons.org/licenses/by-nc-sa/3.0/\">one on this site</a> so that others may learn from your work.\r\n<p role=\"contentinfo\">By <a href=\"http://www.mezzoblue.com/\">Dave Shea</a>. Bandwidth graciously donated by <a href=\"http://www.mediatemple.net/\">mediatemple</a>. Now available: <a href=\"http://www.amazon.com/exec/obidos/ASIN/0321303474/mezzoblue-20/\">Zen Garden, the book</a>.</p>', 'Requirements', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2018-10-28 17:23:42', '2018-10-28 17:23:42', '', 11, 'http://localhost/wordpress/11-revision-v1/', 0, 'revision', '', 0),
(16, 1, '2018-10-28 17:38:08', '2018-10-28 17:38:08', '', 'Mid Century Modern', '', 'trash', 'closed', 'closed', '', 'mid-century-modern__trashed', '', '', '2018-10-28 20:58:50', '2018-10-28 20:58:50', '', 0, 'http://localhost/wordpress/?page_id=16', 0, 'page', '', 0),
(17, 1, '2018-10-28 17:38:08', '2018-10-28 17:38:08', '', 'Mid Century Modern', '', 'inherit', 'closed', 'closed', '', '16-revision-v1', '', '', '2018-10-28 17:38:08', '2018-10-28 17:38:08', '', 16, 'http://localhost/wordpress/16-revision-v1/', 0, 'revision', '', 0),
(18, 1, '2018-10-28 17:38:20', '2018-10-28 17:38:20', '', 'Garments', '', 'trash', 'closed', 'closed', '', 'garments__trashed', '', '', '2018-10-28 20:58:50', '2018-10-28 20:58:50', '', 0, 'http://localhost/wordpress/?page_id=18', 0, 'page', '', 0),
(19, 1, '2018-10-28 17:38:20', '2018-10-28 17:38:20', '', 'Garments', '', 'inherit', 'closed', 'closed', '', '18-revision-v1', '', '', '2018-10-28 17:38:20', '2018-10-28 17:38:20', '', 18, 'http://localhost/wordpress/18-revision-v1/', 0, 'revision', '', 0),
(20, 1, '2018-10-28 17:38:33', '2018-10-28 17:38:33', '', 'Steel', '', 'trash', 'closed', 'closed', '', 'steel__trashed', '', '', '2018-10-28 20:58:50', '2018-10-28 20:58:50', '', 0, 'http://localhost/wordpress/?page_id=20', 0, 'page', '', 0),
(21, 1, '2018-10-28 17:38:33', '2018-10-28 17:38:33', '', 'Steel', '', 'inherit', 'closed', 'closed', '', '20-revision-v1', '', '', '2018-10-28 17:38:33', '2018-10-28 17:38:33', '', 20, 'http://localhost/wordpress/20-revision-v1/', 0, 'revision', '', 0),
(22, 1, '2018-10-28 17:38:50', '2018-10-28 17:38:50', '', 'Apothecary', '', 'trash', 'closed', 'closed', '', 'apothecary__trashed', '', '', '2018-10-28 20:58:50', '2018-10-28 20:58:50', '', 0, 'http://localhost/wordpress/?page_id=22', 0, 'page', '', 0),
(23, 1, '2018-10-28 17:38:50', '2018-10-28 17:38:50', '', 'Apothecary', '', 'inherit', 'closed', 'closed', '', '22-revision-v1', '', '', '2018-10-28 17:38:50', '2018-10-28 17:38:50', '', 22, 'http://localhost/wordpress/22-revision-v1/', 0, 'revision', '', 0),
(24, 1, '2018-10-28 17:39:05', '2018-10-28 17:39:05', '', 'Screen Filler', '', 'trash', 'closed', 'closed', '', 'screen-filler__trashed', '', '', '2018-10-28 20:58:50', '2018-10-28 20:58:50', '', 0, 'http://localhost/wordpress/?page_id=24', 0, 'page', '', 0),
(25, 1, '2018-10-28 17:39:05', '2018-10-28 17:39:05', '', 'Screen Filler', '', 'inherit', 'closed', 'closed', '', '24-revision-v1', '', '', '2018-10-28 17:39:05', '2018-10-28 17:39:05', '', 24, 'http://localhost/wordpress/24-revision-v1/', 0, 'revision', '', 0),
(26, 1, '2018-10-28 17:39:21', '2018-10-28 17:39:21', '', 'Fountain Kiss', '', 'trash', 'closed', 'closed', '', 'fountain-kiss__trashed', '', '', '2018-10-28 20:58:50', '2018-10-28 20:58:50', '', 0, 'http://localhost/wordpress/?page_id=26', 0, 'page', '', 0),
(27, 1, '2018-10-28 17:39:21', '2018-10-28 17:39:21', '', 'Fountain Kiss', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2018-10-28 17:39:21', '2018-10-28 17:39:21', '', 26, 'http://localhost/wordpress/26-revision-v1/', 0, 'revision', '', 0),
(28, 1, '2018-10-28 17:39:37', '2018-10-28 17:39:37', '', 'A Robot Named Jimmy', '', 'trash', 'closed', 'closed', '', 'a-robot-named-jimmy__trashed', '', '', '2018-10-28 20:58:50', '2018-10-28 20:58:50', '', 0, 'http://localhost/wordpress/?page_id=28', 0, 'page', '', 0),
(29, 1, '2018-10-28 17:39:37', '2018-10-28 17:39:37', '', 'A Robot Named Jimmy', '', 'inherit', 'closed', 'closed', '', '28-revision-v1', '', '', '2018-10-28 17:39:37', '2018-10-28 17:39:37', '', 28, 'http://localhost/wordpress/28-revision-v1/', 0, 'revision', '', 0),
(30, 1, '2018-10-28 17:39:51', '2018-10-28 17:39:51', '', 'Verde Moderna', '', 'trash', 'closed', 'closed', '', 'verde-moderna__trashed', '', '', '2018-10-28 20:58:50', '2018-10-28 20:58:50', '', 0, 'http://localhost/wordpress/?page_id=30', 0, 'page', '', 0),
(31, 1, '2018-10-28 17:39:51', '2018-10-28 17:39:51', '', 'Verde Moderna', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2018-10-28 17:39:51', '2018-10-28 17:39:51', '', 30, 'http://localhost/wordpress/30-revision-v1/', 0, 'revision', '', 0),
(32, 1, '2018-10-28 17:40:01', '2018-10-28 17:40:01', '<h2>Who we are</h2><p>Our website address is: http://localhost/wordpress.</p><h2>What personal data we collect and why we collect it</h2><h3>Comments</h3><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><h3>Media</h3><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><h3>Contact forms</h3><h3>Cookies</h3><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><p>If you have an account and you log in to this site, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><h3>Embedded content from other websites</h3><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><h3>Analytics</h3><h2>Who we share your data with</h2><h2>How long we retain your data</h2><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><h2>What rights you have over your data</h2><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><h2>Where we send your data</h2><p>Visitor comments may be checked through an automated spam detection service.</p><h2>Your contact information</h2><h2>Additional information</h2><h3>How we protect your data</h3><h3>What data breach procedures we have in place</h3><h3>What third parties we receive data from</h3><h3>What automated decision making and/or profiling we do with user data</h3><h3>Industry regulatory disclosure requirements</h3>', 'Privacy Policy', '', 'inherit', 'closed', 'closed', '', '3-revision-v1', '', '', '2018-10-28 17:40:01', '2018-10-28 17:40:01', '', 3, 'http://localhost/wordpress/3-revision-v1/', 0, 'revision', '', 0),
(161, 1, '2018-10-29 05:02:44', '2018-10-29 05:02:44', '', '', '', 'inherit', 'closed', 'closed', '', '160-revision-v1', '', '', '2018-10-29 05:02:44', '2018-10-29 05:02:44', '', 160, 'http://localhost/wordpress/160-revision-v1/', 0, 'revision', '', 0),
(160, 1, '2018-10-29 05:02:44', '2018-10-29 05:02:44', '', '', '', 'trash', 'closed', 'closed', '', '160__trashed', '', '', '2018-10-29 13:25:59', '2018-10-29 13:25:59', '', 0, 'http://localhost/wordpress/?post_type=headers&#038;p=160', 0, 'headers', '', 0),
(158, 1, '2018-10-29 04:48:02', '2018-10-29 04:48:02', '', '', '', 'publish', 'closed', 'closed', '', '158', '', '', '2018-10-29 05:00:35', '2018-10-29 05:00:35', '', 0, 'http://localhost/wordpress/?post_type=headers&#038;p=158', 0, 'headers', '', 0),
(159, 1, '2018-10-29 04:48:02', '2018-10-29 04:48:02', '', '', '', 'inherit', 'closed', 'closed', '', '158-revision-v1', '', '', '2018-10-29 04:48:02', '2018-10-29 04:48:02', '', 158, 'http://localhost/wordpress/158-revision-v1/', 0, 'revision', '', 0),
(157, 1, '2018-10-29 04:43:49', '2018-10-29 04:43:49', '', 'View All Designs', 'View every submission to the Zen Garden.', 'publish', 'closed', 'closed', '', 'view-all-designs', '', '', '2018-10-29 04:43:49', '2018-10-29 04:43:49', '', 0, 'http://localhost/wordpress/?p=157', 2, 'nav_menu_item', '', 0),
(156, 1, '2018-10-29 04:43:49', '2018-10-29 04:43:49', '', 'Next Designs <span class=\"indicator\">&rsaquo;</span>', '', 'publish', 'closed', 'closed', '', 'next-designs', '', '', '2018-10-29 04:43:49', '2018-10-29 04:43:49', '', 0, 'http://localhost/wordpress/?p=156', 1, 'nav_menu_item', '', 0),
(41, 1, '2018-10-28 17:45:13', '2018-10-28 17:45:13', '', 'resources', '', 'publish', 'closed', 'closed', '', 'resources', '', '', '2018-10-28 17:45:13', '2018-10-28 17:45:13', '', 0, 'http://localhost/wordpress/?page_id=41', 0, 'page', '', 0),
(42, 1, '2018-10-28 17:45:13', '2018-10-28 17:45:13', '', 'resources', '', 'inherit', 'closed', 'closed', '', '41-revision-v1', '', '', '2018-10-28 17:45:13', '2018-10-28 17:45:13', '', 41, 'http://localhost/wordpress/41-revision-v1/', 0, 'revision', '', 0),
(43, 1, '2018-10-28 17:45:33', '2018-10-28 17:45:33', '', 'faq', '', 'publish', 'closed', 'closed', '', 'faq', '', '', '2018-10-28 17:45:33', '2018-10-28 17:45:33', '', 0, 'http://localhost/wordpress/?page_id=43', 0, 'page', '', 0),
(44, 1, '2018-10-28 17:45:33', '2018-10-28 17:45:33', '', 'faq', '', 'inherit', 'closed', 'closed', '', '43-revision-v1', '', '', '2018-10-28 17:45:33', '2018-10-28 17:45:33', '', 43, 'http://localhost/wordpress/43-revision-v1/', 0, 'revision', '', 0),
(45, 1, '2018-10-28 17:45:39', '2018-10-28 17:45:39', '', 'submit', '', 'publish', 'closed', 'closed', '', 'submit', '', '', '2018-10-28 17:45:39', '2018-10-28 17:45:39', '', 0, 'http://localhost/wordpress/?page_id=45', 0, 'page', '', 0),
(46, 1, '2018-10-28 17:45:39', '2018-10-28 17:45:39', '', 'submit', '', 'inherit', 'closed', 'closed', '', '45-revision-v1', '', '', '2018-10-28 17:45:39', '2018-10-28 17:45:39', '', 45, 'http://localhost/wordpress/45-revision-v1/', 0, 'revision', '', 0),
(47, 1, '2018-10-28 17:45:58', '2018-10-28 17:45:58', '', 'css', '', 'publish', 'closed', 'closed', '', 'css', '', '', '2018-10-28 17:45:58', '2018-10-28 17:45:58', '', 0, 'http://localhost/wordpress/?page_id=47', 0, 'page', '', 0),
(48, 1, '2018-10-28 17:45:58', '2018-10-28 17:45:58', '', 'css', '', 'inherit', 'closed', 'closed', '', '47-revision-v1', '', '', '2018-10-28 17:45:58', '2018-10-28 17:45:58', '', 47, 'http://localhost/wordpress/47-revision-v1/', 0, 'revision', '', 0),
(49, 1, '2018-10-28 17:46:16', '2018-10-28 17:46:16', '', 'translations', '', 'publish', 'closed', 'closed', '', 'translations', '', '', '2018-10-28 17:46:16', '2018-10-28 17:46:16', '', 0, 'http://localhost/wordpress/?page_id=49', 0, 'page', '', 0),
(50, 1, '2018-10-28 17:46:16', '2018-10-28 17:46:16', '', 'translations', '', 'inherit', 'closed', 'closed', '', '49-revision-v1', '', '', '2018-10-28 17:46:16', '2018-10-28 17:46:16', '', 49, 'http://localhost/wordpress/49-revision-v1/', 0, 'revision', '', 0),
(51, 1, '2018-10-28 17:47:27', '2018-10-28 17:47:27', '', 'Translations', 'View translated versions of this page.', 'publish', 'closed', 'closed', '', '51', '', '', '2018-10-28 18:30:52', '2018-10-28 18:30:52', '', 0, 'http://localhost/wordpress/?p=51', 5, 'nav_menu_item', '', 0),
(52, 1, '2018-10-28 17:47:27', '2018-10-28 17:47:27', '', 'View This Design’s <abbr title=\"Cascading Style Sheets\">CSS</abbr>', 'View the source CSS file of the currently-viewed design.', 'publish', 'closed', 'closed', '', '52', '', '', '2018-10-28 18:30:52', '2018-10-28 18:30:52', '', 0, 'http://localhost/wordpress/?p=52', 1, 'nav_menu_item', '', 0),
(53, 1, '2018-10-28 17:47:27', '2018-10-28 17:47:27', '', 'Submit a Design', 'Send in your own CSS file.', 'publish', 'closed', 'closed', '', '53', '', '', '2018-10-28 18:30:52', '2018-10-28 18:30:52', '', 0, 'http://localhost/wordpress/?p=53', 4, 'nav_menu_item', '', 0),
(54, 1, '2018-10-28 17:47:27', '2018-10-28 17:47:27', '', '<abbr title=\"Frequently Asked Questions\">FAQ</abbr>', 'A list of Frequently Asked Questions about the Zen Garden.', 'publish', 'closed', 'closed', '', '54', '', '', '2018-10-28 18:30:52', '2018-10-28 18:30:52', '', 0, 'http://localhost/wordpress/?p=54', 3, 'nav_menu_item', '', 0),
(55, 1, '2018-10-28 17:47:27', '2018-10-28 17:47:27', '', '<abbr title=\"Cascading Style Sheets\">CSS</abbr> Resources', 'Links to great sites with information on using CSS.', 'publish', 'closed', 'closed', '', '55', '', '', '2018-10-28 18:30:52', '2018-10-28 18:30:52', '', 0, 'http://localhost/wordpress/?p=55', 2, 'nav_menu_item', '', 0),
(56, 1, '2018-10-28 19:35:38', '2018-10-28 19:35:38', '', 'Mid Century Modern', '', 'publish', 'open', 'open', '', 'mid-century-modern', '', '', '2018-10-28 20:52:29', '2018-10-28 20:52:29', '', 0, 'http://localhost/wordpress/?p=56', 0, 'post', '', 0),
(57, 1, '2018-10-28 19:35:38', '2018-10-28 19:35:38', '', 'Mid Century Modern', '', 'inherit', 'closed', 'closed', '', '56-revision-v1', '', '', '2018-10-28 19:35:38', '2018-10-28 19:35:38', '', 56, 'http://localhost/wordpress/56-revision-v1/', 0, 'revision', '', 0),
(58, 1, '2018-10-28 19:35:42', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-10-28 19:35:42', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?p=58', 0, 'post', '', 0),
(59, 1, '2018-10-28 19:37:03', '2018-10-28 19:37:03', '', 'Garments', '', 'publish', 'open', 'open', '', 'garments', '', '', '2018-10-28 20:53:14', '2018-10-28 20:53:14', '', 0, 'http://localhost/wordpress/?p=59', 0, 'post', '', 0),
(60, 1, '2018-10-28 19:37:03', '2018-10-28 19:37:03', '', 'Garments', '', 'inherit', 'closed', 'closed', '', '59-revision-v1', '', '', '2018-10-28 19:37:03', '2018-10-28 19:37:03', '', 59, 'http://localhost/wordpress/59-revision-v1/', 0, 'revision', '', 0),
(61, 1, '2018-10-28 19:40:20', '2018-10-28 19:40:20', '', 'Steel', '', 'publish', 'open', 'open', '', 'steel', '', '', '2018-10-28 20:53:45', '2018-10-28 20:53:45', '', 0, 'http://localhost/wordpress/?p=61', 0, 'post', '', 0),
(62, 1, '2018-10-28 19:40:20', '2018-10-28 19:40:20', '', 'Steel', '', 'inherit', 'closed', 'closed', '', '61-revision-v1', '', '', '2018-10-28 19:40:20', '2018-10-28 19:40:20', '', 61, 'http://localhost/wordpress/61-revision-v1/', 0, 'revision', '', 0),
(63, 1, '2018-10-28 19:40:26', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-10-28 19:40:26', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?p=63', 0, 'post', '', 0),
(64, 1, '2018-10-28 19:45:56', '2018-10-28 19:45:56', '', 'Apothecary', '', 'publish', 'open', 'open', '', 'apothecary', '', '', '2018-10-28 20:54:11', '2018-10-28 20:54:11', '', 0, 'http://localhost/wordpress/?p=64', 0, 'post', '', 0),
(65, 1, '2018-10-28 19:45:56', '2018-10-28 19:45:56', '', 'Apothecary', '', 'inherit', 'closed', 'closed', '', '64-revision-v1', '', '', '2018-10-28 19:45:56', '2018-10-28 19:45:56', '', 64, 'http://localhost/wordpress/64-revision-v1/', 0, 'revision', '', 0),
(66, 1, '2018-10-28 19:46:37', '2018-10-28 19:46:37', '', 'Screen Filler', '', 'publish', 'open', 'open', '', 'screen-filler', '', '', '2018-10-28 20:54:41', '2018-10-28 20:54:41', '', 0, 'http://localhost/wordpress/?p=66', 0, 'post', '', 0),
(67, 1, '2018-10-28 19:46:37', '2018-10-28 19:46:37', '', 'Screen Filler', '', 'inherit', 'closed', 'closed', '', '66-revision-v1', '', '', '2018-10-28 19:46:37', '2018-10-28 19:46:37', '', 66, 'http://localhost/wordpress/66-revision-v1/', 0, 'revision', '', 0),
(68, 1, '2018-10-28 20:49:26', '2018-10-28 20:49:26', '', 'Verde Moderna', '', 'publish', 'open', 'open', '', 'verde-moderna', '', '', '2018-10-28 20:56:03', '2018-10-28 20:56:03', '', 0, 'http://localhost/wordpress/?p=68', 0, 'post', '', 0),
(69, 1, '2018-10-28 20:49:26', '2018-10-28 20:49:26', '', 'Verde Moderna', '', 'inherit', 'closed', 'closed', '', '68-revision-v1', '', '', '2018-10-28 20:49:26', '2018-10-28 20:49:26', '', 68, 'http://localhost/wordpress/68-revision-v1/', 0, 'revision', '', 0),
(70, 1, '2018-10-28 20:50:35', '2018-10-28 20:50:35', '', 'A Robot Named Jimmy', '', 'publish', 'open', 'open', '', 'a-robot-named-jimmy', '', '', '2018-10-28 20:55:45', '2018-10-28 20:55:45', '', 0, 'http://localhost/wordpress/?p=70', 0, 'post', '', 0),
(71, 1, '2018-10-28 20:50:35', '2018-10-28 20:50:35', '', 'A Robot Named Jimmy', '', 'inherit', 'closed', 'closed', '', '70-revision-v1', '', '', '2018-10-28 20:50:35', '2018-10-28 20:50:35', '', 70, 'http://localhost/wordpress/70-revision-v1/', 0, 'revision', '', 0),
(72, 1, '2018-10-28 20:50:39', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-10-28 20:50:39', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?p=72', 0, 'post', '', 0),
(73, 1, '2018-10-28 20:51:44', '2018-10-28 20:51:44', '', 'Fountain Kiss', '', 'publish', 'open', 'open', '', 'fountain-kiss', '', '', '2018-10-28 20:55:22', '2018-10-28 20:55:22', '', 0, 'http://localhost/wordpress/?p=73', 0, 'post', '', 0),
(74, 1, '2018-10-28 20:51:34', '2018-10-28 20:51:34', '', 'Draft created on October 28, 2018 at 8:51 pm', '', 'inherit', 'closed', 'closed', '', '73-revision-v1', '', '', '2018-10-28 20:51:34', '2018-10-28 20:51:34', '', 73, 'http://localhost/wordpress/73-revision-v1/', 0, 'revision', '', 0),
(75, 1, '2018-10-28 20:51:44', '2018-10-28 20:51:44', '', 'Fountain Kiss', '', 'inherit', 'closed', 'closed', '', '73-revision-v1', '', '', '2018-10-28 20:51:44', '2018-10-28 20:51:44', '', 73, 'http://localhost/wordpress/73-revision-v1/', 0, 'revision', '', 0),
(76, 1, '2018-10-28 22:51:49', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-28 22:51:49', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=76', 0, 'headers', '', 0),
(77, 1, '2018-10-28 22:55:14', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-28 22:55:14', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=77', 0, 'headers', '', 0),
(78, 1, '2018-10-28 23:09:49', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-28 23:09:49', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=78', 0, 'headers', '', 0),
(79, 1, '2018-10-28 23:10:21', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-28 23:10:21', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=acf-field-group&p=79', 0, 'acf-field-group', '', 0),
(80, 1, '2018-10-29 00:05:20', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 00:05:20', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=80', 0, 'headers', '', 0),
(81, 1, '2018-10-29 00:05:41', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 00:05:41', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=81', 0, 'headers', '', 0),
(82, 1, '2018-10-29 00:06:14', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 00:06:14', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=82', 0, 'headers', '', 0),
(83, 1, '2018-10-29 00:06:20', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-10-29 00:06:20', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?p=83', 0, 'post', '', 0),
(84, 1, '2018-10-29 00:06:40', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-10-29 00:06:40', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?p=84', 0, 'post', '', 0),
(85, 1, '2018-10-29 00:06:49', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 00:06:49', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=85', 0, 'headers', '', 0),
(86, 1, '2018-10-29 00:09:54', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 00:09:54', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=86', 0, 'headers', '', 0),
(87, 1, '2018-10-29 00:10:31', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 00:10:31', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=87', 0, 'headers', '', 0),
(88, 1, '2018-10-29 00:11:02', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 00:11:02', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=88', 0, 'headers', '', 0),
(89, 1, '2018-10-29 00:14:54', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 00:14:54', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=89', 0, 'headers', '', 0),
(90, 1, '2018-10-29 00:15:22', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 00:15:22', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=90', 0, 'headers', '', 0),
(91, 1, '2018-10-29 00:19:39', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 00:19:39', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=91', 0, 'headers', '', 0),
(92, 1, '2018-10-29 00:20:17', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 00:20:17', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=92', 0, 'headers', '', 0),
(93, 1, '2018-10-29 00:20:22', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 00:20:22', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=93', 0, 'headers', '', 0),
(94, 1, '2018-10-29 00:21:13', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 00:21:13', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=94', 0, 'headers', '', 0),
(95, 1, '2018-10-29 00:21:17', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 00:21:17', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=95', 0, 'headers', '', 0),
(96, 1, '2018-10-29 00:28:17', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 00:28:17', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=96', 0, 'headers', '', 0),
(97, 1, '2018-10-29 00:29:36', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 00:29:36', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=97', 0, 'headers', '', 0),
(98, 1, '2018-10-29 00:30:45', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 00:30:45', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=98', 0, 'headers', '', 0),
(99, 1, '2018-10-29 02:24:54', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 02:24:54', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=99', 0, 'headers', '', 0),
(100, 1, '2018-10-29 02:25:53', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 02:25:53', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=100', 0, 'headers', '', 0),
(101, 1, '2018-10-29 02:36:27', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 02:36:27', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=101', 0, 'headers', '', 0),
(102, 1, '2018-10-29 02:37:34', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 02:37:34', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=102', 0, 'headers', '', 0),
(103, 1, '2018-10-29 02:39:03', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 02:39:03', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=103', 0, 'headers', '', 0),
(104, 1, '2018-10-29 02:39:46', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 02:39:46', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=104', 0, 'headers', '', 0),
(105, 1, '2018-10-29 02:40:39', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 02:40:39', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=105', 0, 'headers', '', 0),
(106, 1, '2018-10-29 02:49:02', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 02:49:02', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=106', 0, 'headers', '', 0),
(107, 1, '2018-10-29 02:49:16', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 02:49:16', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=107', 0, 'headers', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(108, 1, '2018-10-29 02:49:18', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 02:49:18', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=108', 0, 'headers', '', 0),
(109, 1, '2018-10-29 02:50:06', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 02:50:06', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=109', 0, 'headers', '', 0),
(110, 1, '2018-10-29 02:50:09', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 02:50:09', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=110', 0, 'headers', '', 0),
(111, 1, '2018-10-29 02:50:27', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 02:50:27', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=111', 0, 'headers', '', 0),
(112, 1, '2018-10-29 03:04:47', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:04:47', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=112', 0, 'headers', '', 0),
(113, 1, '2018-10-29 03:05:07', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:05:07', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=113', 0, 'headers', '', 0),
(114, 1, '2018-10-29 03:05:08', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:05:08', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=114', 0, 'headers', '', 0),
(115, 1, '2018-10-29 03:06:16', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:06:16', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=115', 0, 'headers', '', 0),
(116, 1, '2018-10-29 03:06:26', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:06:26', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=116', 0, 'headers', '', 0),
(117, 1, '2018-10-29 03:07:47', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:07:47', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=117', 0, 'headers', '', 0),
(118, 1, '2018-10-29 03:09:46', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:09:46', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=118', 0, 'headers', '', 0),
(119, 1, '2018-10-29 03:10:28', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:10:28', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=119', 0, 'headers', '', 0),
(120, 1, '2018-10-29 03:10:53', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:10:53', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=120', 0, 'headers', '', 0),
(121, 1, '2018-10-29 03:11:39', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:11:39', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=121', 0, 'headers', '', 0),
(122, 1, '2018-10-29 03:12:10', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:12:10', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=122', 0, 'headers', '', 0),
(123, 1, '2018-10-29 03:12:39', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:12:39', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=123', 0, 'headers', '', 0),
(124, 1, '2018-10-29 03:13:27', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:13:27', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=124', 0, 'headers', '', 0),
(125, 1, '2018-10-29 03:13:47', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:13:47', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=125', 0, 'headers', '', 0),
(126, 1, '2018-10-29 03:14:50', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:14:50', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=126', 0, 'headers', '', 0),
(127, 1, '2018-10-29 03:15:19', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:15:19', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=127', 0, 'headers', '', 0),
(128, 1, '2018-10-29 03:15:51', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:15:51', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=128', 0, 'headers', '', 0),
(129, 1, '2018-10-29 03:16:29', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:16:29', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=129', 0, 'headers', '', 0),
(130, 1, '2018-10-29 03:16:41', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:16:41', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=130', 0, 'headers', '', 0),
(131, 1, '2018-10-29 03:16:53', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:16:53', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=131', 0, 'headers', '', 0),
(132, 1, '2018-10-29 03:17:19', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:17:19', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=132', 0, 'headers', '', 0),
(133, 1, '2018-10-29 03:18:13', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:18:13', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=133', 0, 'headers', '', 0),
(134, 1, '2018-10-29 03:21:00', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:21:00', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=134', 0, 'headers', '', 0),
(135, 1, '2018-10-29 03:21:54', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:21:54', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=135', 0, 'headers', '', 0),
(136, 1, '2018-10-29 03:25:28', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:25:28', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=136', 0, 'headers', '', 0),
(137, 1, '2018-10-29 03:27:22', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:27:22', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=137', 0, 'headers', '', 0),
(138, 1, '2018-10-29 03:27:38', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:27:38', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=138', 0, 'headers', '', 0),
(139, 1, '2018-10-29 03:27:52', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:27:52', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=139', 0, 'headers', '', 0),
(140, 1, '2018-10-29 03:28:18', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:28:18', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=140', 0, 'headers', '', 0),
(141, 1, '2018-10-29 03:28:32', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:28:32', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=141', 0, 'headers', '', 0),
(142, 1, '2018-10-29 03:32:41', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:32:41', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=142', 0, 'headers', '', 0),
(143, 1, '2018-10-29 03:33:01', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:33:01', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=143', 0, 'headers', '', 0),
(144, 1, '2018-10-29 03:33:25', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:33:25', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=144', 0, 'headers', '', 0),
(145, 1, '2018-10-29 03:33:42', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:33:42', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=145', 0, 'headers', '', 0),
(146, 1, '2018-10-29 03:34:07', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 03:34:07', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=146', 0, 'headers', '', 0),
(147, 1, '2018-10-29 03:50:33', '2018-10-29 03:50:33', 'A demonstration of what can be accomplished through <abbr title=\"Cascading Style Sheets\">CSS</abbr>-based design. Select any style sheet from the list to load it into this page.\r\n\r\nDownload the example <a title=\"This page\'s source HTML code, not to be modified.\" href=\"/examples/index\">html file</a> and <a title=\"This page\'s sample CSS, the file you may modify.\" href=\"/examples/style.css\">css file</a>', '', '', 'publish', 'open', 'open', '', '147-2', '', '', '2018-10-29 03:50:56', '2018-10-29 03:50:56', '', 0, 'http://localhost/wordpress/?p=147', 0, 'post', '', 0),
(148, 1, '2018-10-29 03:43:33', '2018-10-29 03:43:33', 'A demonstration of what can be accomplished through <abbr title=\"Cascading Style Sheets\">CSS</abbr>-based design. Select any style sheet from the list to load it into this page.\r\n\r\nDownload the example <a title=\"This page\'s source HTML code, not to be modified.\" href=\"/examples/index\">html file</a> and <a title=\"This page\'s sample CSS, the file you may modify.\" href=\"/examples/style.css\">css file</a>', '', '', 'inherit', 'closed', 'closed', '', '147-revision-v1', '', '', '2018-10-29 03:43:33', '2018-10-29 03:43:33', '', 147, 'http://localhost/wordpress/147-revision-v1/', 0, 'revision', '', 0),
(149, 1, '2018-10-29 03:44:51', '2018-10-29 03:44:51', 'Littering a dark and dreary road lay the past relics of browser-specific tags, incompatible <abbr title=\"Document Object Model\">DOM</abbr>s, broken <abbr title=\"Cascading Style Sheets\">CSS</abbr> support, and abandoned browsers.\r\n\r\nWe must clear the mind of the past. Web enlightenment has been achieved thanks to the tireless efforts of folk like the <abbr title=\"World Wide Web Consortium\">W3C</abbr>, <abbr title=\"Web Standards Project\">WaSP</abbr>, and the major browser creators.\r\n\r\nThe CSS Zen Garden invites you to relax and meditate on the important lessons of the masters. Begin to see with clarity. Learn to use the time-honored techniques in new and invigorating fashion. Become one with the web.', 'The Road to Enlightenment', '', 'publish', 'open', 'open', '', 'the-road-to-enlightenment', '', '', '2018-10-29 03:48:25', '2018-10-29 03:48:25', '', 0, 'http://localhost/wordpress/?p=149', 0, 'post', '', 0),
(150, 1, '2018-10-29 03:44:51', '2018-10-29 03:44:51', 'Littering a dark and dreary road lay the past relics of browser-specific tags, incompatible <abbr title=\"Document Object Model\">DOM</abbr>s, broken <abbr title=\"Cascading Style Sheets\">CSS</abbr> support, and abandoned browsers.\r\n\r\nWe must clear the mind of the past. Web enlightenment has been achieved thanks to the tireless efforts of folk like the <abbr title=\"World Wide Web Consortium\">W3C</abbr>, <abbr title=\"Web Standards Project\">WaSP</abbr>, and the major browser creators.\r\n\r\nThe CSS Zen Garden invites you to relax and meditate on the important lessons of the masters. Begin to see with clarity. Learn to use the time-honored techniques in new and invigorating fashion. Become one with the web.', 'The Road to Enlightenment', '', 'inherit', 'closed', 'closed', '', '149-revision-v1', '', '', '2018-10-29 03:44:51', '2018-10-29 03:44:51', '', 149, 'http://localhost/wordpress/149-revision-v1/', 0, 'revision', '', 0),
(151, 1, '2018-10-29 04:06:02', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 04:06:02', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=151', 0, 'headers', '', 0),
(152, 1, '2018-10-29 04:07:33', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 04:07:33', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=152', 0, 'headers', '', 0),
(153, 1, '2018-10-29 04:07:59', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 04:07:59', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=153', 0, 'headers', '', 0),
(154, 1, '2018-10-29 04:13:16', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 04:13:16', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=154', 0, 'headers', '', 0),
(155, 1, '2018-10-29 04:13:30', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 04:13:30', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=155', 0, 'headers', '', 0),
(162, 1, '2018-10-29 13:33:46', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 13:33:46', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=162', 0, 'headers', '', 0),
(163, 1, '2018-10-29 13:34:03', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 13:34:03', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=163', 0, 'headers', '', 0),
(164, 1, '2018-10-29 13:37:04', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 13:37:04', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=164', 0, 'headers', '', 0),
(165, 1, '2018-10-29 13:37:07', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 13:37:07', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=165', 0, 'headers', '', 0),
(166, 1, '2018-10-29 13:37:10', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 13:37:10', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=166', 0, 'headers', '', 0),
(167, 1, '2018-10-29 13:37:11', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 13:37:11', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=167', 0, 'headers', '', 0),
(168, 1, '2018-10-29 13:37:25', '2018-10-29 13:37:25', '', '', '', 'publish', 'closed', 'closed', '', '168', '', '', '2018-10-29 13:37:57', '2018-10-29 13:37:57', '', 0, 'http://localhost/wordpress/?post_type=headers&#038;p=168', 0, 'headers', '', 0),
(169, 1, '2018-10-29 13:37:25', '2018-10-29 13:37:25', '', '', '', 'inherit', 'closed', 'closed', '', '168-revision-v1', '', '', '2018-10-29 13:37:25', '2018-10-29 13:37:25', '', 168, 'http://localhost/wordpress/168-revision-v1/', 0, 'revision', '', 0),
(170, 1, '2018-10-29 13:38:02', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 13:38:02', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=170', 0, 'headers', '', 0),
(171, 1, '2018-10-29 13:38:13', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 13:38:13', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=171', 0, 'headers', '', 0),
(172, 1, '2018-10-29 13:38:32', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-29 13:38:32', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress/?post_type=headers&p=172', 0, 'headers', '', 0),
(173, 1, '2018-10-29 13:39:04', '2018-10-29 13:39:04', '', '', '', 'publish', 'closed', 'closed', '', '173', '', '', '2018-10-29 14:31:12', '2018-10-29 14:31:12', '', 0, 'http://localhost/wordpress/?post_type=headers&#038;p=173', 0, 'headers', '', 0),
(174, 1, '2018-10-29 13:39:04', '2018-10-29 13:39:04', '', '', '', 'inherit', 'closed', 'closed', '', '173-revision-v1', '', '', '2018-10-29 13:39:04', '2018-10-29 13:39:04', '', 173, 'http://localhost/wordpress/173-revision-v1/', 0, 'revision', '', 0),
(175, 1, '2018-10-29 14:27:47', '2018-10-29 14:27:47', '', 'enso', '', 'inherit', 'open', 'closed', '', 'enso', '', '', '2018-10-29 14:27:47', '2018-10-29 14:27:47', '', 0, 'http://localhost/wordpress/wp-content/uploads/2018/10/enso.png', 0, 'attachment', 'image/png', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

DROP TABLE IF EXISTS `wp_termmeta`;
CREATE TABLE IF NOT EXISTS `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

DROP TABLE IF EXISTS `wp_terms`;
CREATE TABLE IF NOT EXISTS `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'benefits', 'benefits', 0),
(3, 'participation', 'participation', 0),
(4, 'explanation', 'explanation', 0),
(5, 'requirements', 'requirements', 0),
(6, 'benefits', 'benefits', 0),
(7, 'side main nav', 'side-main-nav', 0),
(8, 'main nav bar', 'main-nav-bar', 0),
(9, 'design', 'design', 0),
(10, 'post-format-aside', 'post-format-aside', 0),
(11, 'summary', 'summary', 0),
(12, 'preamble', 'preamble', 0),
(13, 'post-format-quote', 'post-format-quote', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

DROP TABLE IF EXISTS `wp_term_relationships`;
CREATE TABLE IF NOT EXISTS `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(6, 3, 0),
(8, 2, 0),
(11, 5, 0),
(13, 4, 0),
(157, 7, 0),
(156, 7, 0),
(52, 8, 0),
(55, 8, 0),
(54, 8, 0),
(53, 8, 0),
(51, 8, 0),
(56, 1, 0),
(59, 1, 0),
(61, 1, 0),
(147, 11, 0),
(64, 1, 0),
(64, 10, 0),
(66, 1, 0),
(66, 10, 0),
(61, 10, 0),
(59, 10, 0),
(56, 10, 0),
(68, 1, 0),
(68, 10, 0),
(70, 1, 0),
(70, 10, 0),
(73, 1, 0),
(73, 10, 0),
(149, 13, 0),
(149, 12, 0),
(147, 13, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

DROP TABLE IF EXISTS `wp_term_taxonomy`;
CREATE TABLE IF NOT EXISTS `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 8),
(2, 2, 'category', '', 0, 1),
(3, 3, 'category', '', 0, 1),
(4, 4, 'category', '', 0, 1),
(5, 5, 'category', '', 0, 1),
(6, 6, 'post_tag', '', 0, 0),
(7, 7, 'nav_menu', '', 0, 2),
(8, 8, 'nav_menu', '', 0, 5),
(9, 9, 'category', '', 0, 0),
(10, 10, 'post_format', '', 0, 8),
(11, 11, 'category', '', 0, 1),
(12, 12, 'category', '', 0, 1),
(13, 13, 'post_format', '', 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

DROP TABLE IF EXISTS `wp_usermeta`;
CREATE TABLE IF NOT EXISTS `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'root'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy'),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:1:{s:64:\"039aa9107874b63e37f6d0c06b0e21655bf530a3175fb860850594f7872ed96f\";a:4:{s:10:\"expiration\";i:1540897822;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36\";s:5:\"login\";i:1540725022;}}'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '4'),
(18, 1, 'managenav-menuscolumnshidden', 'a:1:{i:0;s:11:\"description\";}'),
(25, 1, 'closedpostboxes_acf-field-group', 'a:0:{}'),
(19, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:12:\"add-post_tag\";i:1;s:15:\"add-post_format\";}'),
(20, 1, 'wp_user-settings', 'editor=tinymce'),
(21, 1, 'wp_user-settings-time', '1540743738'),
(22, 1, 'closedpostboxes_post', 'a:0:{}'),
(23, 1, 'metaboxhidden_post', 'a:6:{i:0;s:11:\"postexcerpt\";i:1;s:13:\"trackbacksdiv\";i:2;s:16:\"commentstatusdiv\";i:3;s:11:\"commentsdiv\";i:4;s:7:\"slugdiv\";i:5;s:9:\"authordiv\";}'),
(24, 1, 'nav_menu_recently_edited', '7'),
(27, 1, 'closedpostboxes_headers', 'a:0:{}'),
(26, 1, 'metaboxhidden_acf-field-group', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(28, 1, 'metaboxhidden_headers', 'a:1:{i:0;s:7:\"slugdiv\";}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

DROP TABLE IF EXISTS `wp_users`;
CREATE TABLE IF NOT EXISTS `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'root', '$P$B6tFTUVoQ7OOb8XjCg7aY.NOFUEIcE0', 'root', 'samohero12@yahoo.com', '', '2018-10-28 11:10:07', '', 0, 'root');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php

  require get_template_directory() .'/lib/assets.php';
  require get_template_directory() .'/lib/custom.php';
  require get_template_directory() .'/lib/nav.php';
  require get_template_directory() .'/lib/widgets.php';
  require get_template_directory() .'/lib/init.php';
  require get_template_directory() .'/lib/metaBoxes.php';

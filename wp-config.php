<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'mysql');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Bz!%S%>9@ud{gOEl#9To5!z.Y$(sWVSYo^MKcGvN*+1p!Jv6,$Fh})4BbY^+GG}F');
define('SECURE_AUTH_KEY',  'fd,`;)FF`d=sW7t@IlP(qOI?5`(,a$R.JyS-_Ezt_TO}gC5fE^$|L|X9CZ^K ot.');
define('LOGGED_IN_KEY',    'rP5NfCyXFd`_{P<gn*n[h!;?<ks/;9.cO;U00Q*`Os>qp)xR>R*M}*Xbg:)z}|)T');
define('NONCE_KEY',        'FrPL_L1BOz0Bo?;YAV_G3)n5|b =,6>>C`hH@(M#A0B-t!`G{c(`|KN2RtX-<^wB');
define('AUTH_SALT',        'L#3{(8F~( yY(usfv?J<BzUD5lW=JAIltg-uO=4vUkDS!*IfN+~G.umA%?=_`|U/');
define('SECURE_AUTH_SALT', '0@$g!%dVr=pi.T(8#ZZc=UW_~w<,6wNgZj8}a~>8ECegO5?tn3{$:fRI37KH{nN1');
define('LOGGED_IN_SALT',   '*`k(pTy*2}0^Olt@0Tt`LM83v^3e[(cb}R8Uee.>#&?h#vf)QecR9*lh9Vb{!Vxv');
define('NONCE_SALT',       'w#3dn=gq-1gM+M{W0rQ#+:~<w7N3ag#unwcwnh6Lu9wopYC2p*-*]KGLCSTq4oMu');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
